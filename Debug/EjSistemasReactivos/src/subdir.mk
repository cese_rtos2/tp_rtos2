################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../EjSistemasReactivos/src/leds.c 

OBJS += \
./EjSistemasReactivos/src/leds.o 

C_DEPS += \
./EjSistemasReactivos/src/leds.d 


# Each subdirectory must supply rules for building sources it contributes
EjSistemasReactivos/src/%.o EjSistemasReactivos/src/%.su: ../EjSistemasReactivos/src/%.c EjSistemasReactivos/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F429xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I"/home/gaston/rtos_2TP/tp_rtos2/Application/Inc" -I"/home/gaston/rtos_2TP/tp_rtos2/Supporting_Functions/Inc" -I"/home/gaston/rtos_2TP/tp_rtos2/Supporting_Functions/Src" -I"/home/gaston/rtos_2TP/tp_rtos2/EjSistemasReactivos/inc" -I"/home/gaston/rtos_2TP/tp_rtos2/EjSistemasReactivos/src" -I"/home/gaston/rtos_2TP/tp_rtos2/EjSistemasReactivos" -I"/home/gaston/rtos_2TP/17co2022-examples/lib_telegraph/inc" -I"/home/gaston/rtos_2TP/17co2022-examples/lib_telegraph/Debug" -I"/home/gaston/rtos_2TP/17co2022-examples/lib_telegraph/src" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-EjSistemasReactivos-2f-src

clean-EjSistemasReactivos-2f-src:
	-$(RM) ./EjSistemasReactivos/src/leds.d ./EjSistemasReactivos/src/leds.o ./EjSistemasReactivos/src/leds.su

.PHONY: clean-EjSistemasReactivos-2f-src

