################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Application/Src/MessageList_c.c \
../Application/Src/blinky_ao.c \
../Application/Src/bsp.c \
../Application/Src/button_ao.c \
../Application/Src/debouncer_ao.c \
../Application/Src/messages.c \
../Application/Src/recorder_ao.c \
../Application/Src/routine_example_command.c \
../Application/Src/rtos_ao.c \
../Application/Src/singly_linked_c.c \
../Application/Src/uart_ao.c \
../Application/Src/uart_interrupt_driver.c 

OBJS += \
./Application/Src/MessageList_c.o \
./Application/Src/blinky_ao.o \
./Application/Src/bsp.o \
./Application/Src/button_ao.o \
./Application/Src/debouncer_ao.o \
./Application/Src/messages.o \
./Application/Src/recorder_ao.o \
./Application/Src/routine_example_command.o \
./Application/Src/rtos_ao.o \
./Application/Src/singly_linked_c.o \
./Application/Src/uart_ao.o \
./Application/Src/uart_interrupt_driver.o 

C_DEPS += \
./Application/Src/MessageList_c.d \
./Application/Src/blinky_ao.d \
./Application/Src/bsp.d \
./Application/Src/button_ao.d \
./Application/Src/debouncer_ao.d \
./Application/Src/messages.d \
./Application/Src/recorder_ao.d \
./Application/Src/routine_example_command.d \
./Application/Src/rtos_ao.d \
./Application/Src/singly_linked_c.d \
./Application/Src/uart_ao.d \
./Application/Src/uart_interrupt_driver.d 


# Each subdirectory must supply rules for building sources it contributes
Application/Src/%.o Application/Src/%.su: ../Application/Src/%.c Application/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F429xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I"/home/gaston/rtos_2TP/tp_rtos2/Application/Inc" -I"/home/gaston/rtos_2TP/tp_rtos2/Supporting_Functions/Inc" -I"/home/gaston/rtos_2TP/tp_rtos2/Supporting_Functions/Src" -I"/home/gaston/rtos_2TP/tp_rtos2/EjSistemasReactivos/inc" -I"/home/gaston/rtos_2TP/tp_rtos2/EjSistemasReactivos/src" -I"/home/gaston/rtos_2TP/tp_rtos2/EjSistemasReactivos" -I"/home/gaston/rtos_2TP/17co2022-examples/lib_telegraph/inc" -I"/home/gaston/rtos_2TP/17co2022-examples/lib_telegraph/Debug" -I"/home/gaston/rtos_2TP/17co2022-examples/lib_telegraph/src" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Application-2f-Src

clean-Application-2f-Src:
	-$(RM) ./Application/Src/MessageList_c.d ./Application/Src/MessageList_c.o ./Application/Src/MessageList_c.su ./Application/Src/blinky_ao.d ./Application/Src/blinky_ao.o ./Application/Src/blinky_ao.su ./Application/Src/bsp.d ./Application/Src/bsp.o ./Application/Src/bsp.su ./Application/Src/button_ao.d ./Application/Src/button_ao.o ./Application/Src/button_ao.su ./Application/Src/debouncer_ao.d ./Application/Src/debouncer_ao.o ./Application/Src/debouncer_ao.su ./Application/Src/messages.d ./Application/Src/messages.o ./Application/Src/messages.su ./Application/Src/recorder_ao.d ./Application/Src/recorder_ao.o ./Application/Src/recorder_ao.su ./Application/Src/routine_example_command.d ./Application/Src/routine_example_command.o ./Application/Src/routine_example_command.su ./Application/Src/rtos_ao.d ./Application/Src/rtos_ao.o ./Application/Src/rtos_ao.su ./Application/Src/singly_linked_c.d ./Application/Src/singly_linked_c.o ./Application/Src/singly_linked_c.su ./Application/Src/uart_ao.d ./Application/Src/uart_ao.o ./Application/Src/uart_ao.su ./Application/Src/uart_interrupt_driver.d ./Application/Src/uart_interrupt_driver.o ./Application/Src/uart_interrupt_driver.su

.PHONY: clean-Application-2f-Src

