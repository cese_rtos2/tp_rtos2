#include "leds.h"
#include "bsp.h"
#include "supporting_Functions.h"
//#include "rtos_ao.h"
static StackType_t leds_stack[configMINIMAL_STACK_SIZE * 5]; /* Task stack */
static Event *leds_queue[20];
static Leds leds ;
Active *AO_leds = &leds.super ;
///! all off -> verde -> rojo ->azul ->all off

State Leds_all_off(Leds *const me, const Event * const e) ;
State Leds_green_On(Leds *const me, const Event * const e) ;
State Leds_red_On(Leds *const me, const Event * const e) ;
State Leds_blue_On(Leds *const me, const Event * const e) ;



State Leds_initial(Leds *const me, const Event * const e){
	return TRAN(Leds_all_off) ;
}

State Leds_all_off(Leds *const me, const Event * const e) {
	State status ;
	switch(e->sig){
		case  ENTRY_SIG:
			BSP_Led1_TurnOff() ;
			BSP_Led2_TurnOff() ;
			BSP_Led3_TurnOff() ;
			status = HANDLED_STATUS ;
			break ;
		case STATE_CHANGE_SIG:
			status = TRAN(Leds_green_On) ;
			break ;
		default:
			status = IGNORED_STATUS ;
			break ;
	}

	return status ;
}

State Leds_green_On(Leds *const me, const Event * const e){
	State status ;

	switch(e->sig){
		case ENTRY_SIG:
			BSP_Led1_TurnOn() ;
			status = HANDLED_STATUS  ;
			break ;
		case STATE_CHANGE_SIG:
			status = TRAN(Leds_red_On) ;
			break ;

		default:
			status = IGNORED_STATUS ;
	}
	return status ;

}


State Leds_red_On(Leds *const me, const Event * const e) {
	State status ;
	vPrintString("LEDS_RED_ON\r\n") ;

	switch(e->sig){
			case ENTRY_SIG:
				BSP_Led2_TurnOn() ;
				status = HANDLED_STATUS  ;
				break ;
			case STATE_CHANGE_SIG:
				status = TRAN(Leds_blue_On) ;
				break ;

			default:
				status = IGNORED_STATUS ;
		}
		return status ;

}


State Leds_blue_On(Leds *const me, const Event * const e) {
	State status ;
	switch(e->sig){
			case ENTRY_SIG:
				BSP_Led3_TurnOn() ;
				status = HANDLED_STATUS  ;
				break ;
			case STATE_CHANGE_SIG:
				status = TRAN(Leds_all_off) ;
				break ;
			default:
				status = IGNORED_STATUS ;
		}
		return status ;
}










void Leds_ctor(Leds *const me) {
	Active_ctor(&me->super,(StateHandler)&Leds_initial) ;

}



void Leds_start(uint8_t priority, uint16_t opt){
	Leds_ctor(&leds) ;
	Active_start(AO_leds,
	             priority,
	             leds_queue,
	             sizeof(leds_queue)/sizeof(leds_queue[0]),
	             leds_stack,
	             sizeof(leds_stack)/sizeof(leds_stack[0]),
	             opt);

}
