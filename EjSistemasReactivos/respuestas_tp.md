## Respuestas: 

La forma de realizar el mismo ejercicio sin usar freeRtos, usando frameworks de test puede llegar a ser la siguiente: 
- Realizar los test sobre los cambios de estado 
- Activar y enviar mensajes sobre los objetos activos para visualizar los cambios de estado. 
- Usando punteros a funciones. Otra posibilidad es usar freeRtos en C para gnu linux/windows dado por el soporte oficial de freertos. (ver pag web) 

Ejercicio resuelto: 
 init -> Leds_all_off ->ev: STATE_CHANGE_SIG -> Leds_green_On ->ev: STATE_CHANGE_SIG ->Leds_red_On->ev:STATE_CHANGE_SIG -> Leds_blue_On - >ev:Leds_all_off



