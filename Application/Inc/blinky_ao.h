#ifndef INC_BLINKY_AO_H_
#define INC_BLINKY_AO_H_

#include "rtos_ao.h"

enum BlinkySignals {
    BLINKY_TIMEOUT_SIG = USER_SIG
};

typedef struct Blinky {
    Active super; /* inherit Active base class */

    TimeEvent te;
} Blinky;

extern Active *AO_blinky;

void Blinky_start(uint8_t priority, uint16_t opt);
State Blinky_initial(Blinky * const me, const Event * const e);
State Blinky_off(Blinky * const me, const Event * const e);
State Blinky_on(Blinky * const me, const Event * const e);

#endif /* INC_BLINKY_AO_H_ */
