#ifndef INC_MESSAGELIST_C_H_
#define INC_MESSAGELIST_C_H_

#include "singly_linked_c.h"

#include <stdint.h>
#include <stdlib.h>

typedef enum MessageErrorCode {
	MESSAGE_OK,
	MESSAGE_ERROR
}MessageErrorCode;

typedef struct MessageNode {
    SLNode super;
    uint8_t *msg;
    size_t len;
    size_t size; /* bytes */
}MessageNode;

MessageErrorCode MessageNode_setMsgSize(MessageNode * const me, size_t size);
MessageErrorCode MessageNode_newMessage(MessageNode * const me);
MessageErrorCode MessageNode_deleteMessage(MessageNode * const me);
MessageNode * const MessageNode_new(void);
MessageErrorCode MessageNode_delete(MessageNode ** const me);
void MessageNode_ctor (MessageNode * const me, SLNode * const next, uint8_t *msg, size_t len, size_t size);

typedef struct MessageListNode {
	SLNode super;
	SLList list;
}MessageListNode;

MessageListNode * const MessageListNode_new(void);
MessageErrorCode MessageListNode_delete(MessageListNode ** const me);
void MessageListNode_ctor (MessageListNode * const me, SLNode * const next, SLNode * const head);

typedef struct MessageList {
	SLList super;
}MessageList;

MessageList * const MessageList_new(void);
MessageErrorCode MessageList_delete(MessageList ** const me);
void MessageList_ctor (MessageList * const me, SLNode * const head);

#endif /* INC_MESSAGELIST_C_H_ */
