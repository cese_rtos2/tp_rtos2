#ifndef INC_BSP_H_
#define INC_BSP_H_

#include <stdint.h>
#include "main.h"
void BSP_UserButton_Off(void) ;
void BSP_UserButton_On(void) ;
void BSP_Led1_TurnOn (void);
void BSP_Led1_TurnOff (void);
void BSP_Led1_Toggle (void);
void BSP_Led2_TurnOn (void);
void BSP_Led2_TurnOff (void);
void BSP_Led2_Toggle (void);
void BSP_Led3_TurnOn (void);
void BSP_Led3_TurnOff (void);
void BSP_Led3_Toggle (void);
uint8_t BSP_UserButton_Read (void);

#endif /* INC_BSP_H_ */
