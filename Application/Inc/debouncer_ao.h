#ifndef INC_DEBOUNCER_AO_H_
#define INC_DEBOUNCER_AO_H_

#include "rtos_ao.h"

enum DebouncerSignals {
    DEBOUNCER_TIMEOUT_SIG = USER_SIG,
	DEBOUNCER_ANY_EDGE_SIG,
	DEBOUNCER_FALLING_EDGE_SIG,
	DEBOUNCER_RISING_EDGE_SIG
};

typedef struct Debouncer {
    Active super;
    TimeEvent te;
} Debouncer;

extern Active *AO_debouncer;

void Debouncer_start(uint8_t priority, uint16_t opt);
State Debouncer_initial(Debouncer * const me, const Event * const e);
State Debouncer_high(Debouncer * const me, const Event * const e);
State Debouncer_falling(Debouncer * const me, const Event * const e);
State Debouncer_low(Debouncer * const me, const Event * const e);
State Debouncer_rising(Debouncer * const me, const Event * const e);

#endif /* INC_DEBOUNCER_AO_H_ */
