#ifndef INC_UART_AO_H_
#define INC_UART_AO_H_

#include "rtos_ao.h"
#include "singly_linked_c.h"
#include "MessageList_c.h"
enum uartSignals {
	UART_START_SIG = USER_SIG,
	UART_START_BTN_SIG,
	UART_PLAY_SIG,
	UART_STOP_SIG,
	UART_STOP_BTN_SIG,
	UART_LIST_SIG,
	UART_SIZE_SIG,
	UART_SAVE_SIG,
	UART_DELETE_SIG,

	UART_START_TIMEOUT_SIG,
	UART_STOP_TIMEOUT_SIG,


};

typedef struct Uart {
    Active super; /* inherit Active base class */
    TimeEvent start;
    TimeEvent stop;

} Uart;

extern Uart uart ;
extern Active *AO_uart;
extern MessageListNode *pointer_last_msg ;

void Uart_start(uint8_t priority, uint16_t opt);
State Uart_initial(Uart * const me, const Event * const e);
State Uart_waiting(Uart * const me, const Event * const e);
State Uart_command_start(Uart * const me, const Event * const e);
State Uart_command_save(Uart * const me, const Event * const e) ;
void unlock_semaphore_handled() ;

#endif /* INC_UART_AO_H_ */
