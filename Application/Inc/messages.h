#ifndef INC_MESSAGES_H_
#define INC_MESSAGES_H_

extern int *pointer_to_init_msg ;


int message_recording_start(void);
const void * message_recording_stop(void);
int message_save(int *list_index, const void *message);
int message_play(int list_index);
int message_delete(int list_index);
int * message_list(void);
size_t message_list_size(void);
int index_message_playing() ;
#endif /* INC_MESSAGES_H_ */
