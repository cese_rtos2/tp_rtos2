#ifndef INC_BUTTON_AO_H_
#define INC_BUTTON_AO_H_

#include "rtos_ao.h"

enum ButtonSignals {
    BUTTON_PRESSED_SIG = USER_SIG,
	BUTTON_RELEASED_SIG
};

typedef struct Button {
    Active super; /* inherit Active base class */
} Button;


extern Active *AO_button;

void Button_start(uint8_t priority, uint16_t opt);
State Button_initial(Button * const me, const Event * const e);
State Button_released(Button * const me, const Event * const e);
State Button_pressed(Button * const me, const Event * const e);

#endif /* INC_BUTTON_AO_H_ */
