#ifndef INC_RECORDER_AO_H_
#define INC_RECORDER_AO_H_

#include "rtos_ao.h"
#include "MessageList_c.h"

#include "appConfigs.h"

#define RECORDER_MESSAGE_CHUNK_SIZE (__RECORDER_MESSAGE_CHUNK_SIZE) /*Specified in: bytes*/
#define RECORDER_RECORDING_DELAY (__RECORDER_RECORDING_DELAY) /*Specified in: ms*/
#define RECORDER_SAMPLE_RATE (__RECORDER_SAMPLE_RATE) /*Specified in: ms*/

/*----------Recorder facilities----------*/
enum RecorderSignals {
    RECORDER_TIMEOUT_SIG = USER_SIG,
	RECORDER_SAMPLE_SIG,
	RECORDER_PRESSED_SIG,
	RECORDER_RELEASED_SIG,
	RECORDER_NEW_CHUNK_SIG,
	RECORDER_GET_NEXT_CHUNK_SIG,
	RECORDER_STOP_SIG,
	RECORDER_START_UART_SIG,
	RECORDER_PLAYING_MSG_SIG,
};

typedef struct Recorder {
    Active super;
    TimeEvent te, sample;
    MessageList msgList;
} Recorder;

extern Active *AO_recorder;
extern MessageListNode *currentMsgList ;  ///! using for call to message_save

void  Recorder_start  (uint8_t priority, uint16_t opt);
State Recorder_initial(Recorder * const me, const Event * const e);
State Recorder_standBy(Recorder * const me, const Event * const e);
State Recorder_recording(Recorder * const me, const Event * const e);
State Recorder_playing(Recorder * const me, const Event * const e);

#endif /* INC_RECORDER_AO_H_ */
