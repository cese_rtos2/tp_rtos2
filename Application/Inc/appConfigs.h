#ifndef INC_APPCONFIGS_H_
#define INC_APPCONFIGS_H_

#define RECORDINGDELAY_MS	3000
#define RECORDINGSAMPLERATE_MS	20
#define RECORDINGBUFFERLEN	2560

#define __RECORDER_MESSAGE_CHUNK_SIZE 500
#define __RECORDER_RECORDING_DELAY 3000
#define __RECORDER_SAMPLE_RATE 20
//#define EJ_SISTEMAS_REACTIVOS
#endif /* INC_APPCONFIGS_H_ */
