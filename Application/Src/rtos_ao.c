#include <rtos_ao.h>

/*---------------------------------------------------------------------------*/
/* Finite State Machine facilities... */
static Event const entryEvt = {ENTRY_SIG};
static Event const exitEvt = {EXIT_SIG};

void Fsm_ctor(Fsm * const me, StateHandler initial) {
    me->state = initial;
}

void Fsm_init(Fsm * const me, const Event * const e) {
    configASSERT(me->state != (StateHandler)0);
    (*me->state)(me, e); /* We always asume TRAN_STATUS */
    (*me->state)(me, &entryEvt);
}

void Fsm_dispatch(Fsm * const me, const Event * const e) {
    State stat;
    StateHandler prev_state = me->state; /* save for later */
    assert(me->state != (StateHandler)0);
    stat = (*me->state)(me, e);

    if (stat == TRAN_STATUS) { /* transition taken? */
        (*prev_state)(me, &exitEvt);
        (*me->state)(me, &entryEvt);
    }
}

/*---------------------------------------------------------------------------*/
/* Actvie Object facilities... */
void Active_ctor(Active * const me, StateHandler initial) {
    Fsm_ctor(&me->super, initial);
}

/* Event-loop thread function for all Active Objects (FreeRTOS task signature) */
static void Active_eventLoop(void *pvParameters) {
    Active *me = (Active *)pvParameters;

    assert(me); /* Active object must be provided */

    /* initialize the AO */
    Fsm_init(&me->super, (Event *)0);

    for (;;) {   /* for-ever "superloop" */
        Event *e; /* pointer to event object ("message") */

        /* wait for any event and receive it into object 'e' */
        xQueueReceive(me->queue, &e, portMAX_DELAY); /* BLOCKING! */

        /* dispatch event to the active object 'me' */
        Fsm_dispatch(&me->super, e); /* NO BLOCKING! */
    }
}

void Active_start(Active * const me,
                  uint8_t prio,       /* priority (1-based) */
                  Event **queueSto,
                  uint32_t queueLen,
                  void *stackSto,
                  uint32_t stackSize,
                  uint16_t opt)
{
    StackType_t *stk_sto = stackSto;
    uint32_t stk_depth = (stackSize / sizeof(StackType_t));

    (void)opt; /* unused parameter */
    me->queue = xQueueCreateStatic(
                   queueLen,            /* queue length - provided by user */
                   sizeof(Event *),     /* item size */
                   (uint8_t *)queueSto, /* queue storage - provided by user */
                   &me->queue_cb);      /* queue control block */
    assert(me->queue); /* queue must be created */

    me->thread = xTaskCreateStatic(
              &Active_eventLoop,        /* the thread function */
              "AO" ,                    /* the name of the task */
              stk_depth,                /* stack depth */
              me,                       /* the 'pvParameters' parameter */
              prio + tskIDLE_PRIORITY,  /* FreeRTOS priority */
              stk_sto,                  /* stack storage - provided by user */
              &me->thread_cb);          /* task control block */
    assert(me->thread); /* thread must be created */
}


void Active_post(Active * const me, Event const * const e) {
    BaseType_t status = xQueueSend(me->queue, (void *)&e, (TickType_t)0);
    //configASSERT(status == pdTRUE); /* Evaluate if its necessary to reset on failure */
    assert(status == pdTRUE);
    //(void)status;
}

void Active_postFromISR(Active * const me, Event const * const e,
                        BaseType_t *pxHigherPriorityTaskWoken)
{

    BaseType_t status = xQueueSendFromISR(me->queue, (void *)&e,
                                          pxHigherPriorityTaskWoken);
    //    configASSERT(status == pdTRUE);
    assert(status == pdTRUE);

    //(void)status;
}

/*--------------------------------------------------------------------------*/
/* Time Event services... */
/* When in doubt, use brute force! */

static TimeEvent *l_tevt[10]; /* all TimeEvents in the application */
static uint_fast8_t l_tevtNum; /* current number of TimeEvents */

void TimeEvent_ctor(TimeEvent * const me, Signal sig, Active *act) {
    /* no critical section because it is presumed that all TimeEvents
    * are created *before* multitasking has started.
    */
    me->super.sig = sig;
    me->act = act;
    me->timeout = 0U;
    me->interval = 0U;

    /* register one more TimeEvent with the application */
    assert(l_tevtNum < sizeof(l_tevt)/sizeof(l_tevt[0]));
    l_tevt[l_tevtNum] = me;
    ++l_tevtNum;
}

void TimeEvent_arm(TimeEvent * const me, uint32_t timeout, uint32_t interval) {
    portDISABLE_INTERRUPTS();
    me->timeout = timeout;
    me->interval = interval;
    portENABLE_INTERRUPTS();
}

void TimeEvent_disarm(TimeEvent * const me) {
    portDISABLE_INTERRUPTS();
    me->timeout = 0U;
    portENABLE_INTERRUPTS();
}

void TimeEvent_tickFromISR(BaseType_t *pxHigherPriorityTaskWoken) {
    uint_fast8_t i;
	  for (i = 0U; i < l_tevtNum; ++i) {
	  TimeEvent * const t = l_tevt[i];
	  assert(t); /* TimeEvent instance must be registered */
	  if (t->timeout > 0U) { /* is this TimeEvent armed? */
	      if (--t->timeout == 0U) { /* is it expiring now? */
	          Active_postFromISR(t->act, &t->super,
	                             pxHigherPriorityTaskWoken);
	          t->timeout = t->interval; /* rearm or disarm (one-shot) */
	     }
	  }
	 }
}
