#include <debouncer_ao.h>
#include "appConfigs.h"
#include "button_ao.h"
#include "bsp.h"
//#include "leds.h"
//#include "supporting_Functions.h"

static StackType_t debouncer_stack[configMINIMAL_STACK_SIZE * 5]; /* Task stack */
static Event *debouncer_queue[15];
static Debouncer debouncer;
Active *AO_debouncer = &debouncer.super;

static const uint32_t debounceTimeout = 20;

State Debouncer_initial(Debouncer * const me, const Event * const e){
	return TRAN(Debouncer_low);
}

State Debouncer_high(Debouncer * const me, const Event * const e){
	State status;
	switch (e->sig) {
		case ENTRY_SIG: {
			TimeEvent_disarm(&me->te);
			status = HANDLED_STATUS;
			break;
		}
		case EXIT_SIG: {
            TimeEvent_arm(&me->te, debounceTimeout, 0);
			status = HANDLED_STATUS;
			break;
		}
		case DEBOUNCER_ANY_EDGE_SIG: {
			if (BSP_UserButton_Read() == 0) {
				status = TRAN(Debouncer_falling);
			} else {
				status = HANDLED_STATUS;
			}
			break;
		}
		default: {
			status = IGNORED_STATUS;
			break;
		}
	}
	return status;
}

State Debouncer_falling(Debouncer * const me, const Event * const e){
	State status;

	switch (e->sig) {
		case DEBOUNCER_TIMEOUT_SIG: {
			if (BSP_UserButton_Read() == 0) {
				/* Falling edge event */
#ifndef EJ_SISTEMAS_REACTIVOS
				static const Event e = {BUTTON_RELEASED_SIG};
        		Active_post(AO_button, &e);
#endif
        		status = TRAN(Debouncer_low);
			} else {
				status = TRAN(Debouncer_high);
			}
			break;
		}
		default: {
			status = IGNORED_STATUS;
			break;
		}
	}
	return status;
}

State Debouncer_low(Debouncer * const me, const Event * const e){
	State status;

	switch (e->sig) {
		case ENTRY_SIG: {
			TimeEvent_disarm(&me->te);
			status = HANDLED_STATUS;
			break;
		}
		case EXIT_SIG: {
			TimeEvent_arm(&me->te, debounceTimeout, 0);
			status = HANDLED_STATUS;
			break;
		}
		case DEBOUNCER_ANY_EDGE_SIG: {
			if (BSP_UserButton_Read() == 1) {
				status = TRAN(Debouncer_rising);
			} else {
				status = HANDLED_STATUS;
			}
			break;
		}
		default: {
			status = IGNORED_STATUS;
			break;
		}
	}
	return status;
}

State Debouncer_rising(Debouncer * const me, const Event * const e){
	State status;

	switch (e->sig) {
		case DEBOUNCER_TIMEOUT_SIG: {
			if (BSP_UserButton_Read() == 1) {
				/* Rising edge event */
#ifdef EJ_SISTEMAS_REACTIVOS
				static const Event e = { STATE_CHANGE_SIG } ;
				Active_post(AO_leds, &e);
#else
				static const Event e = {BUTTON_PRESSED_SIG};
				Active_post(AO_button, &e);

#endif
				status = TRAN(Debouncer_high);
			} else {
				status = TRAN(Debouncer_low);
			}
			break;
		}
		default: {
			status = IGNORED_STATUS;
			break;
		}
	}
	return status;
}

static void Debouncer_ctor(Debouncer * const me) {
    Active_ctor(&me->super, (StateHandler)&Debouncer_initial);
    TimeEvent_ctor(&me->te, DEBOUNCER_TIMEOUT_SIG, &me->super);
}

void Debouncer_start(uint8_t priority, uint16_t opt) {
    /* create and start the Debouncer AO */
    Debouncer_ctor(&debouncer);
    Active_start(AO_debouncer,
                priority,
                debouncer_queue,
                sizeof(debouncer_queue)/sizeof(debouncer_queue[0]),
                debouncer_stack,
                sizeof(debouncer_stack)/sizeof(debouncer_stack[0]),
                opt);
}
