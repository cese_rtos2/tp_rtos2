#include <recorder_ao.h>
#include "uart_ao.h"
#include "bsp.h"
#include "messages.h"
#include "supporting_Functions.h"
#include <stdbool.h>
//#include <testingApp.h>
/*----------Recorder facilities----------*/
static StackType_t recorder_stack[configMINIMAL_STACK_SIZE * 15]; /* Task stack */
static Event *recorder_queue[20];
static Recorder recorder;
Active *AO_recorder = &recorder.super;

static const uint32_t recordingDelay = RECORDER_RECORDING_DELAY;
static const uint32_t sampleRate = RECORDER_SAMPLE_RATE;

MessageListNode *currentMsgList = NULL;
static MessageNode *currentMsg = NULL;

State Recorder_playing_node(Recorder *const me, Event const *e )  ;





State Recorder_initial(Recorder * const me, const Event * const e){
	BSP_Led1_TurnOn();
	return TRAN(Recorder_standBy);
}

State Recorder_standBy(Recorder * const me, const Event * const e){
	State status;
	static bool recordingRdy = false;
	const Event evStartUart = {UART_START_BTN_SIG } ;
	switch (e->sig) {
		case RECORDER_PRESSED_SIG: {
			TimeEvent_arm(&me->te, recordingDelay, 0);
			status = HANDLED_STATUS;
			break;
		}
		case RECORDER_RELEASED_SIG: {
			if(recordingRdy == true){
				recordingRdy = false;
				Active_post(AO_uart,&evStartUart) ;
				status = TRAN(Recorder_recording);
			} else {
				TimeEvent_disarm(&me->te);
				status = HANDLED_STATUS;
			}
			break;
		}
		case RECORDER_TIMEOUT_SIG: {
            recordingRdy = true;
			status = HANDLED_STATUS;
			break;
		}case RECORDER_START_UART_SIG: {
			status = TRAN(Recorder_recording);
			break;
		}case RECORDER_PLAYING_MSG_SIG: {
			TimeEvent_disarm(&me->te) ;
			status = TRAN(Recorder_playing_node);
			break;
		}
		default: {
			status = IGNORED_STATUS;
			break;
		}
	}
	return status;
}

State Recorder_recording(Recorder * const me, const Event * const e){
	State status;
	static const Event newChunkSig = {RECORDER_NEW_CHUNK_SIG};

	switch (e->sig) {
		case ENTRY_SIG: {
			BSP_Led2_TurnOn();
			currentMsgList = MessageListNode_new();
			assert(currentMsgList) ;
			MessageListNode_ctor(currentMsgList, NULL, NULL);
			TimeEvent_arm(&me->sample, sampleRate, sampleRate);
			TimeEvent_arm(&me->te, recordingDelay, 0);
			Active_post(AO_recorder, &newChunkSig);
			break;
		}
		case EXIT_SIG: {
			TimeEvent_disarm(&me->sample);
			BSP_Led2_TurnOff();
			status = HANDLED_STATUS;
			break;
		}
		case RECORDER_NEW_CHUNK_SIG: {
			currentMsg = MessageNode_new();
			assert(currentMsg) ;
			MessageNode_ctor(currentMsg, NULL, NULL, 0, RECORDER_MESSAGE_CHUNK_SIZE);
			SLList_append(&currentMsgList->list, (SLNode *)currentMsg);
			if (MESSAGE_OK != MessageNode_newMessage(currentMsg)) {
				status = TRAN(Recorder_standBy);
			}else{
				status = HANDLED_STATUS;
			}
			break;
		}
		case RECORDER_SAMPLE_SIG: {
			currentMsg->msg[currentMsg->len] = BSP_UserButton_Read();
			if(currentMsg->size == ++currentMsg->len){
				Active_post(AO_recorder, &newChunkSig);
			}
			status = HANDLED_STATUS;
			break;
		}
		case RECORDER_PRESSED_SIG: {
			TimeEvent_disarm(&me->te);
			status = HANDLED_STATUS;
			break;
		}
		case RECORDER_RELEASED_SIG: {
			TimeEvent_arm(&me->te, recordingDelay, 0);
			status = HANDLED_STATUS;
			break;
		}
		case RECORDER_TIMEOUT_SIG: {
			status = TRAN(Recorder_playing);
			//!change for transitions for display messages using uart !
			break;
		}case RECORDER_STOP_SIG: {
			status = TRAN(Recorder_playing);//
			//!change for transitions for display messages using uart !
			break;
		}
		default: {
			status = IGNORED_STATUS;
			break;
		}
	}
	return status;
}

State Recorder_playing(Recorder * const me, const Event * const e){
	///! DISABLE BUTTON !
	State status;
	static MessageListNode *currentMsgList_new = NULL;
//	static MessageNode *currentMsg = NULL;
	static uint32_t messageIdx = 0;
	static uint32_t currentMsgIdx = 0;
	//uint32_t cant_messages ;
	static const Event getNextChunkSig = {RECORDER_GET_NEXT_CHUNK_SIG};
	static const Event evSAVEstate = {UART_STOP_BTN_SIG} ;
	switch (e->sig) {
		case ENTRY_SIG: {
			currentMsgList_new =  (MessageListNode *)SLList_getNode((SLList *)&currentMsgList,  SLList_getLength((SLList *)&currentMsgList) -1 ) ;
			TimeEvent_arm(&me->sample, sampleRate, sampleRate);
			status = HANDLED_STATUS;
			Active_post(AO_recorder, &getNextChunkSig);
			break;
		}
		case EXIT_SIG: {
			BSP_UserButton_On() ;
			TimeEvent_disarm(&me->sample);
			Active_post(AO_uart,&evSAVEstate) ;
			status = HANDLED_STATUS;
			break;
		}
		case RECORDER_GET_NEXT_CHUNK_SIG: {
			if(currentMsgIdx >= SLList_getLength(&currentMsgList_new->list)){
				currentMsgIdx = 0;
				status = TRAN(Recorder_standBy);
			}else{
				currentMsg = (MessageNode *)SLList_getNode(&currentMsgList_new->list, currentMsgIdx++);
				status = HANDLED_STATUS;
			}
			break;
		}
		case RECORDER_SAMPLE_SIG: {
			if (currentMsg->msg[messageIdx] == 0) {
				BSP_Led3_TurnOff();
			}else{
				BSP_Led3_TurnOn();
			}
			if (++messageIdx >= currentMsg->len) {
				messageIdx = 0;
				Active_post(AO_recorder, &getNextChunkSig);
			}
			status = HANDLED_STATUS;
			break;
		}
		default: {
			status = IGNORED_STATUS;
			break;
		}
	}
	return status;
}



State Recorder_playing_node(Recorder *const me, Event const *e )
{
	State status ;
	static MessageListNode *currentMsgList = NULL;
	static MessageNode *currentMsg = NULL;
	static uint32_t messageIdx = 0;
	static uint32_t currentMsgIdx = 0;
	static const Event getNextChunkSig = {RECORDER_GET_NEXT_CHUNK_SIG};
	switch(e->sig){
		case ENTRY_SIG:
			currentMsgList = (MessageListNode *)SLList_getNode((SLList *)&me->msgList,index_message_playing()-1 );
			TimeEvent_arm(&me->sample, sampleRate, sampleRate);
			status = HANDLED_STATUS;
			Active_post(AO_recorder, &getNextChunkSig);
			break ;
		case EXIT_SIG:
			status =  TRAN(Recorder_standBy);
			break ;
		case RECORDER_GET_NEXT_CHUNK_SIG:
			if(currentMsgIdx >= SLList_getLength(&currentMsgList->list)){
				currentMsgIdx = 0;
				status = TRAN(Recorder_standBy);
			}else{
				currentMsg = (MessageNode *)SLList_getNode(&currentMsgList->list, currentMsgIdx++);
				status = HANDLED_STATUS;
			}
			break ;
		case RECORDER_SAMPLE_SIG: {
			if (currentMsg->msg[messageIdx] == 0) {
					BSP_Led3_TurnOff();
			}else{
				BSP_Led3_TurnOn();
			}
			if (++messageIdx >= currentMsg->len) {
				messageIdx = 0;
				Active_post(AO_recorder, &getNextChunkSig);
			}
			status = HANDLED_STATUS;
			break  ;
		}
		default:
			status = IGNORED_STATUS ;
	}
	return status ;


}

void Recorder_ctor(Recorder * const me)
{
    Active_ctor(&me->super, (StateHandler)&Recorder_initial);
    TimeEvent_ctor(&me->te, RECORDER_TIMEOUT_SIG, &me->super);
    TimeEvent_ctor(&me->sample, RECORDER_SAMPLE_SIG, &me->super);
    MessageList_ctor(&me->msgList, NULL);
	pointer_to_init_msg =(int *)&me->msgList ;
}

void Recorder_start(uint8_t priority, uint16_t opt)
{
    Recorder_ctor(&recorder);
    Active_start(AO_recorder,
                priority,
                recorder_queue,
                sizeof(recorder_queue)/sizeof(recorder_queue[0]),
                recorder_stack,
                sizeof(recorder_stack)/sizeof(recorder_stack[0]),
                opt);
}



