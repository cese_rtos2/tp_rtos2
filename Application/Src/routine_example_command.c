/*
 * Copyright (c) 2022 SEBASTIAN BEDIN <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : routine_example_command.c
 * @date   : May 18, 2022
 * @author : SEBASTIAN BEDIN <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <arch.h>
#include <command.h>
#include <appConfigs.h>
#include <uart_ao.h>
#include "uart_interrupt_driver.h"
#include "routine_example_command.h"
#include "messages.h"
#include <rtos_ao.h>
#include "supporting_Functions.h"
/********************** macros and definitions *******************************/
#define UART_AO_INITIAL  1
#define UART_AO_CHECKING 2
#define UART_AO_START    3
#define UART_AO_SAVE     4

#define STACK_SIZE 200


#define PERIOD_ticks_                   (20 / APP_CONFIG_PERIOD_ms)
#define DELAY_ticks_                    (500 / APP_CONFIG_PERIOD_ms)

#define BUFFER_SIZE_                    (64)
/*
 * 115200 / (10 * 1000) = 11.52 bytes/ms = aprox 16 = 2^4
 * 64 / 16 = 4
 */
#define TIMEOUT_RX_MS_                 (100)
#define TIMEOUT_TX_MS_(len)            ((len) * 20)

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/
 StaticTask_t xTaskBuffer;
 StackType_t xStack[ STACK_SIZE ];
 static uint8_t rx_buffer_[BUFFER_SIZE_];
 static uint8_t tx_buffer_[BUFFER_SIZE_];
 static uint8_t user_buffer_[BUFFER_SIZE_];
 static uart_interrupt_driver_t driver_;
 int delay_start = 0 ;
 int delay_stop  = 0  ;
 int play_number_msg = 0 ;
 int delete_number_messages = 0 ;
/********************** external data definition *****************************/

extern UART_HandleTypeDef huart3;

/********************** internal functions definition ************************/




void initTaskUart(uint8_t priority, uint16_t opt){
    TaskHandle_t xHandle = NULL;
    xHandle = xTaskCreateStatic(
                          loop_,       /* Function that implements the task. */
                          "uart_rx",          /* Text name for the task. */
                          STACK_SIZE,      /* Number of indexes in the xStack array. */
                          NULL,    /* Parameter passed into the task. */
                          priority,/* Priority at which the task is created. */
                          xStack,          /* Array to use as the task's stack. */
                          &xTaskBuffer );
    assert(xHandle!=NULL) ;

}



void init_(void *parameters) {
	uart_interrupt_driver_config_t config;
	command_init();
	config.uart = &huart3;
	config.rx.buffer = rx_buffer_;
	config.rx.buffer_size = BUFFER_SIZE_;
	config.tx.buffer = tx_buffer_;
	config.tx.buffer_size = BUFFER_SIZE_;
	uart_interrupt_driver_init(&driver_, &config);

}





void loop_(void *parameters)
{
	init_(NULL) ;
	for(;;){
		uint16_t size = uart_interrupt_driver_receive(&driver_, user_buffer_, portMAX_DELAY) ;
		//if (decoder==true ) {}
		for (uint16_t i = 0; i < size; ++i)
		{
		   command_char_in(user_buffer_[i]); ////! call to handler messages
		}
		clean_buffer_rx(&driver_) ;
	}
}






static void uart_string_write_(const char *str) {
  size_t len = strlen(str);
  HAL_StatusTypeDef tx_status;
  tx_status = HAL_UART_Transmit(&huart3, (uint8_t*)str, len, TIMEOUT_TX_MS_(len));
  if (HAL_OK != tx_status) {
	  ///FIXME: añadir fflush para buffer de transmisión
  }
}


/********************** external functions definition ************************/

////! void decision_state_handler -> true o false !
///static void send_active_post( Active_post(AO_uart,ev)

static void send_active_post(Event e) {

	Active_post(AO_uart, &e)  ;


}


void command_start_handler(unsigned int delay) {
	delay_start = delay  ;
	const Event e = {UART_START_SIG} ;
	send_active_post(e);
}

void command_stop_handler(unsigned int delay)
{
	const Event e = {UART_STOP_SIG} ;
	delay_stop = delay ;
	send_active_post(e);
}

void command_save_handler(void) {
	const Event e = {UART_SAVE_SIG} ;
	send_active_post(e);

}

void command_play_handler(unsigned int id)
{
	const Event e = {UART_PLAY_SIG} ;
	play_number_msg = id ;
	send_active_post(e);
}

void command_delete_handler(unsigned int id) {
	const Event delete_sig = {UART_DELETE_SIG} ;
	delete_number_messages = id ;
	send_active_post(delete_sig) ;

}

void command_list_handler(void) {
	const Event list = {UART_LIST_SIG } ;
	send_active_post(list) ;
}

void command_size_handler(void) {
  uart_string_write_("command_size_handler\n\r");

}



void command_char_out(char ch) {
  HAL_StatusTypeDef tx_status;
  tx_status = HAL_UART_Transmit(&huart3, (uint8_t *)&ch, 1, TIMEOUT_TX_MS_(20));

  if (HAL_OK != tx_status) {
    // error
    uart_string_write_("Error");
  }
}

//// manage of isr
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
//	uart_interrupt_driver_tx_isr(&driver_, huart);
}



void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size)
{
  uart_interrupt_driver_rx_isr(&driver_, huart, Size);
}




/********************** end of file ******************************************/
