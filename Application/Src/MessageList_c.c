#include "MessageList_c.h"
#include "FreeRTOS.h"

MessageErrorCode MessageNode_setMsgSize(MessageNode * const me, size_t size) {
	me->size = size;
	return MESSAGE_OK;
}

MessageErrorCode MessageNode_newMessage(MessageNode * const me) {
	me->msg = (uint8_t *)pvPortMalloc(me->size);
	if(NULL == me->msg) return MESSAGE_ERROR;
	return MESSAGE_OK;
}

MessageErrorCode MessageNode_deleteMessage(MessageNode * const me) {
	if(NULL == me->msg) return MESSAGE_OK;
	vPortFree(me->msg);
	me->msg = NULL;
	me->len = 0;
	return MESSAGE_OK;
}

MessageNode * const MessageNode_new(void) {
	return (MessageNode *)pvPortMalloc(sizeof(MessageNode));
}

MessageErrorCode MessageNode_delete(MessageNode ** const me) {
	if(NULL == (*me)) return MESSAGE_OK;
	MessageNode_deleteMessage(*me);
	vPortFree((*me));
	(*me) = NULL;
	return MESSAGE_OK;
}

void MessageNode_ctor (MessageNode * const me, SLNode * const next, uint8_t *msg, size_t len, size_t size){
	SLNode_ctor((SLNode *)me, next);
	me->msg = msg;
	me->len = len;
	me->size = size;
}

MessageListNode * const MessageListNode_new(void) {
	return (MessageListNode *)pvPortMalloc(sizeof(MessageListNode));
}

MessageErrorCode MessageListNode_delete(MessageListNode ** const me) {
	if(NULL == (*me)) return MESSAGE_OK;
	for(int idx = (SLList_getLength(&(*me)->list) - 1); idx >= 0; --idx){
		MessageNode *node = (MessageNode *)SLList_removeByIndex(&(*me)->list, idx);
		MessageNode_delete(&node);
	}
	vPortFree((*me));
	(*me) = NULL;
	return MESSAGE_OK;
}

void MessageListNode_ctor (MessageListNode * const me, SLNode * const next, SLNode * const head){
	SLNode_ctor((SLNode *)me, next);
	SLList_ctor((SLList *)&me->list, head);
}

MessageList * const MessageList_new(void) {
	return (MessageList *)pvPortMalloc(sizeof(MessageList));
}

MessageErrorCode MessageList_delete(MessageList ** const me) {
	if(NULL == (*me)) return MESSAGE_OK;
	for(int idx = (SLList_getLength((SLList *)(*me) - 1)); idx >= 0; --idx){
		MessageListNode *node = (MessageListNode *)SLList_removeByIndex((SLList *)(*me), idx);
		MessageListNode_delete(&node);
	}
	vPortFree((*me));
	(*me) = NULL;
	return MESSAGE_OK;
}

void MessageList_ctor (MessageList * const me, SLNode * const head) {
	SLList_ctor((SLList *)me, head);
}
