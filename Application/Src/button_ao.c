#include <button_ao.h>
#include "recorder_ao.h"
#include "bsp.h"

static StackType_t button_stack[configMINIMAL_STACK_SIZE * 5]; /* Task stack */
static Event *button_queue[5];
static Button button;
Active *AO_button = &button.super;

State Button_initial(Button * const me, const Event * const e) {
    return TRAN(Button_released);
}

State Button_released(Button * const me, const Event * const e) {
    State status;
    switch (e->sig) {
        case BUTTON_PRESSED_SIG: {
            status = TRAN(Button_pressed);
            break;
        }
        default: {
            status = IGNORED_STATUS;
            break;
        }
    }
    return status;
}

State Button_pressed(Button * const me, const Event * const e) {
    State status;
    switch (e->sig) {
        case ENTRY_SIG: {
        	BSP_Led3_TurnOn();
        	static const Event e = {RECORDER_PRESSED_SIG};
        	Active_post(AO_recorder, &e);
            status = HANDLED_STATUS;
            break;
        }
        case EXIT_SIG: {
        	BSP_Led3_TurnOff();
        	static const Event e = {RECORDER_RELEASED_SIG};
        	Active_post(AO_recorder, &e);
            status = HANDLED_STATUS;
            break;
        }
        case BUTTON_RELEASED_SIG: {
            status = TRAN(Button_released);
            break;
        }
        default: {
            status = IGNORED_STATUS;
            break;
        }
    }
    return status;
}

static void Button_ctor(Button * const me) {
    Active_ctor(&me->super, (StateHandler)&Button_initial);
}

void Button_start(uint8_t priority, uint16_t opt) {
    /* create and start the Button AO */
	Button_ctor(&button);
    Active_start(AO_button,
                priority,
                button_queue,
                sizeof(button_queue)/sizeof(button_queue[0]),
                button_stack,
                sizeof(button_stack)/sizeof(button_stack[0]),
                opt);
}
