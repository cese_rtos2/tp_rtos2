/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "messages.h"
#include "recorder_ao.h"
#include "supporting_Functions.h"

#define STATE_RECORDER_STANDBY_AO_MESSAGES   1
#define STATE_RECORDER_RECORDING_AO_MESSAGES 2   //Recorder_recording
#define STATE_RECORDER_PLAYING_AO_MESSAGES   3
#define STATE_RECORDER_AO_MESSAGES_ERROR     4

static int status_recorder_ao() ;
static void messages_ctor_func() ;
int index_message_playing() ;

static int index_message_playing_ ;
int *pointer_to_init_msg = NULL ;


Active *AO_messages ; //= *AO_recorder ;

int message_recording_start(void) {
	int8_t error_code =  STATE_RECORDER_AO_MESSAGES_ERROR ;
	static const Event e = {RECORDER_START_UART_SIG};
	messages_ctor_func() ;
	error_code = status_recorder_ao() ;
	if (error_code == STATE_RECORDER_STANDBY_AO_MESSAGES){
		error_code = 0 ;
		Active_post(AO_recorder,&e) ; ///send signal using uart

	}else if (error_code == STATE_RECORDER_RECORDING_AO_MESSAGES){
		error_code = 0 ;
	}else error_code = -1  ;

	return error_code;
}


///! detener la grabación en cualquier momento independiente de los
/// tres segundos de inactividad
const void * message_recording_stop(void) {
	MessageListNode *pointer_to_msg ;
	uint8_t error_code;
	messages_ctor_func() ;
	error_code = status_recorder_ao() ;
	if (error_code == STATE_RECORDER_STANDBY_AO_MESSAGES){
		pointer_to_msg = 	(MessageListNode *)SLList_getNode((SLList *)&currentMsgList,  SLList_getLength((SLList *)&currentMsgList) -1 ) ;
	}else if (error_code == STATE_RECORDER_PLAYING_AO_MESSAGES){
		pointer_to_msg = 	(MessageListNode *)SLList_getNode((SLList *)&currentMsgList,  SLList_getLength((SLList *)&currentMsgList) -1 ) ;

	}else {
		pointer_to_msg = NULL ;
	}

//	(MessageListNode *)SLList_getNode((SLList *)&currentMsgList,  SLList_getLength((SLList *)&currentMsgList) -1 ) ;
	return pointer_to_msg  ;
}


/**
 * @brief boton o start guardar el mensaje o descartarlo
 * @param list_index:
 * @param message
 * @return
 */
int message_save(int *list_index, const void *message) {

	SLList_append((SLList *)&list_index, (SLNode *)message );
	return 0;
}

/* Reproducir mensaje en la lista de mensajes. Retorna errCode */

int message_play(int list_index) {
	int error_code = -1 ;
	static const Event e = {RECORDER_PLAYING_MSG_SIG} ;
	if (list_index > message_list_size()){
		return error_code ;
	}
	messages_ctor_func() ;
	if (status_recorder_ao() == STATE_RECORDER_STANDBY_AO_MESSAGES){
		error_code = 0 ;
		index_message_playing_  = list_index ;
		Active_post(AO_recorder , &e) ;
				///active_post ! event: RECORDER_POINTER_MSG_SIG
	}
	return error_code;
}

/**
 *
 * @param list_index: indice en la lista -> borrar un mensaje de la lista
 * @return error code
 */

int message_delete(int list_index) {
	///! if exist messages ->
	uint8_t cant_messages = message_list_size() ;
	MessageListNode * delete_node = NULL ;
	if (cant_messages <0 || cant_messages<=list_index){
		return -1 ;
	}
	delete_node = (MessageListNode *)SLList_getNode((SLList *)&pointer_to_init_msg,list_index-1 );
	if (delete_node == NULL){
		return -1 ;
	}
	if (MessageListNode_delete( delete_node) == MESSAGE_OK){
		return 0;
	}
	return -1;
}

/**
 *
 * @return puntero al inicio del mensaje
 */
int * message_list(void) {

	return pointer_to_init_msg ;


}

/* Devolver cantidad de elementos en la lista de mensajes. Retorna cantidad de elementos*/
size_t message_list_size(void) {
	int cant_messages  = SLList_getLength((SLList *)pointer_to_init_msg) ;
	return cant_messages ;
}

static void messages_ctor_func(){
	AO_messages = AO_recorder ; ///! copia de struct de datos de recorder_ao

}


static int status_recorder_ao(){
	int8_t error_code = STATE_RECORDER_AO_MESSAGES_ERROR ;
	if(AO_messages->super.state == (StateHandler )&Recorder_standBy){
		error_code = STATE_RECORDER_STANDBY_AO_MESSAGES ;
	}else if (AO_messages->super.state == (StateHandler )&Recorder_recording){
		error_code = STATE_RECORDER_RECORDING_AO_MESSAGES ;
	}else if (AO_messages->super.state == (StateHandler )&Recorder_playing){
		error_code = STATE_RECORDER_PLAYING_AO_MESSAGES ;
	}else{
		error_code = STATE_RECORDER_AO_MESSAGES_ERROR ;
	}
	return error_code ;
}

int index_message_playing() {
	return index_message_playing_ ;
}

