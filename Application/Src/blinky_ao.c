#include <blinky_ao.h>
#include <main.h>
#include <button_ao.h>

enum OBLedState {
    LED_ON,
    LED_OFF
};

static StackType_t blinky_stack[configMINIMAL_STACK_SIZE * 2]; /* Task stack */
static Event *blinky_queue[5];
static Blinky blinky;
Active *AO_blinky = &blinky.super;

State Blinky_initial(Blinky * const me, const Event * const e) {
    TimeEvent_arm(&me->te, (1000 / portTICK_PERIOD_MS), (1000 / portTICK_PERIOD_MS));
    return TRAN(Blinky_off);
}

State Blinky_off(Blinky * const me, const Event * const e) {
    State status;
    switch (e->sig) {
        case ENTRY_SIG: {
        	status = HANDLED_STATUS;
            break;
        }
        case BLINKY_TIMEOUT_SIG: {
            status = TRAN(Blinky_on);
            break;
        }
        default: {
            status = IGNORED_STATUS;
            break;
        }
    }
    return status;
}

State Blinky_on(Blinky * const me, const Event * const e) {
    State status;
    switch (e->sig) {
        case ENTRY_SIG: {
        	status = HANDLED_STATUS;
            break;
        }
        case BLINKY_TIMEOUT_SIG: {
            status = TRAN(Blinky_off);
            break;
        }
        default: {
            status = IGNORED_STATUS;
            break;
        }
    }
    return status;
}

static void Blinky_ctor(Blinky * const me) {
    Active_ctor(&me->super, (StateHandler)&Blinky_initial);
    TimeEvent_ctor(&me->te, BLINKY_TIMEOUT_SIG, &me->super);
}

void Blinky_start(uint8_t priority, uint16_t opt) {
    /* create and start the Blinky AO */
    Blinky_ctor(&blinky);
    Active_start(AO_blinky,
                priority,
                blinky_queue,
                sizeof(blinky_queue)/sizeof(blinky_queue[0]),
                blinky_stack,
                sizeof(blinky_stack)/sizeof(blinky_stack[0]),
                opt);
}
