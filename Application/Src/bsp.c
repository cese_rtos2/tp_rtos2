#include "bsp.h"

enum OnBoardLedState {
    LED_OFF,
    LED_ON
};

void BSP_UserButton_Off(void){
	///disable isr bnt
}

void BSP_UserButton_On(void){
	///enable isr bnt
}


void BSP_Led1_TurnOn (void) {
	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, LED_ON);
}
void BSP_Led1_TurnOff (void) {
	HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, LED_OFF);
}
void BSP_Led1_Toggle (void) {
	((uint8_t)HAL_GPIO_ReadPin(LED1_GPIO_Port, LED1_Pin) == LED_OFF) ? BSP_Led1_TurnOn() : BSP_Led1_TurnOff();
}
void BSP_Led2_TurnOn (void) {
	HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, LED_ON);
}
void BSP_Led2_TurnOff (void) {
	HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, LED_OFF);
}
void BSP_Led2_Toggle (void) {
	((uint8_t)HAL_GPIO_ReadPin(LED2_GPIO_Port, LED2_Pin) == LED_OFF) ? BSP_Led2_TurnOn() : BSP_Led2_TurnOff();
}
void BSP_Led3_TurnOn (void) {
	HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, LED_ON);
}
void BSP_Led3_TurnOff (void) {
	HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, LED_OFF);
}
void BSP_Led3_Toggle (void) {
	((uint8_t)HAL_GPIO_ReadPin(LED3_GPIO_Port, LED3_Pin) == LED_OFF) ? BSP_Led3_TurnOn() : BSP_Led3_TurnOff();
}
uint8_t BSP_UserButton_Read (void) {
	return (uint8_t)HAL_GPIO_ReadPin(Button_User_GPIO_Port, Button_User_Pin);
}
