#include "supporting_Functions.h"
#include <stdio.h>
#include "recorder_ao.h"
#include "routine_example_command.h"
#include <uart_ao.h>
#include <main.h>
#include <button_ao.h>
////! libraries for MessageListNode
#include "FreeRTOS.h"
#include "singly_linked_c.h"
#include "MessageList_c.h"
///! ------------------------------
#include "messages.h"
#define BUFFER_SIZE_                    (64)

static StackType_t uart_stack[configMINIMAL_STACK_SIZE * 20]; /* Task stack */
static Event *uart_queue[20];
Uart uart;
Active *AO_uart = &uart.super;
MessageListNode *pointer_last_msg ;



extern UART_HandleTypeDef huart3;



State Uart_initial(Uart * const me, const Event * const e) {
    return TRAN(Uart_waiting);
}

State Uart_waiting(Uart * const me, const Event * const e)
{
	State status;
//	static const Event evUARTSTART = {RECORDER_START_UART_SIG};
	vPrintString("UART_WAITING\r\n") ;
	switch(e->sig)
	{
		case UART_START_SIG:
			TimeEvent_arm(&me->start, delay_start, 0) ;
			delay_start = 0  ;
			status = HANDLED_STATUS ;
			break ;
		case UART_START_BTN_SIG:
			status = TRAN(Uart_command_start);
			break ;
		case UART_PLAY_SIG:
			if (play_number_msg >0){
				if (message_play(play_number_msg) == -1){
					vPrintString("el numero de mensaje no existe\r\n") ;
				}
			}else vPrintString("error el numero de mensajes deber ser mayor a cero") ;
			status = HANDLED_STATUS ;
			break ;
		case UART_LIST_SIG:
		 	vPrintStringAndNumber("numero de mensajes: " ,message_list_size()) ;
			status = HANDLED_STATUS ;
			break ;
		case UART_DELETE_SIG:
			vPrintString("uart_delete_signal\r\n") ;
			if (delete_number_messages>0){
				if (message_delete(delete_number_messages)==1){
					vPrintString("mensaje borrado\r\n") ;
				}else vPrintString("mensaje no borrado\r\n");
			}
			delete_number_messages = 0 ;
			status = HANDLED_STATUS ;
			break ;
		case UART_SIZE_SIG:
			status = HANDLED_STATUS ;
			break ;
		case UART_START_TIMEOUT_SIG:
			vPrintString("timeout_start \r\n")  ;
			TimeEvent_disarm(&me->start) ;
			message_recording_start() ;
			status = TRAN(Uart_command_start) ;
			break ;
		default:
			status = IGNORED_STATUS ;
	}
	return status;
}


State Uart_command_start(Uart * const me, const Event * const e)
{
	vPrintString("uart_command_start\r\n") ;
	State status;
	switch(e->sig)
	{
		case EXIT_SIG:
			pointer_last_msg =(MessageListNode *) message_recording_stop();
			status = HANDLED_STATUS  ;
			break ;
		case UART_STOP_SIG:
			TimeEvent_arm(&me->stop, delay_stop, 0);
			delay_stop = 0 ;
			status = HANDLED_STATUS ;
			break ;
		case UART_STOP_TIMEOUT_SIG:
			status = TRAN(Uart_command_save) ;
			break ;
		case UART_STOP_BTN_SIG:
			status = TRAN(Uart_command_save) ;
			break ;
		default:
			status = IGNORED_STATUS ;
	}
	return status;
}
////!uart_waiting_playing()


State Uart_command_save(Uart * const me, const Event * const e){

	///! COMMAND - ERROR ->
	State status ;//= HANDLED_STATUS 			   ;
	vPrintString("uart_command_save\r\n") ;

	switch(e->sig)
	{
		case EXIT_SIG:
			status = TRAN(Uart_waiting) ;
			break ;
		case UART_PLAY_SIG:
		case UART_STOP_SIG:
		case UART_LIST_SIG:
		case UART_SIZE_SIG:
		case UART_START_SIG:
		case UART_START_BTN_SIG:
		case UART_STOP_BTN_SIG:
			vPrintString("mensaje borrado") ;
			MessageListNode_delete( &pointer_last_msg) ;
			status = TRAN(Uart_waiting) ;
			break ;
		case UART_SAVE_SIG:
			vPrintString("SAVE MESSAGE\r\n")  ;
			message_save(pointer_to_init_msg, pointer_last_msg) ;			break ;
			status = TRAN(Uart_waiting) ;
			break ;
		default:
			status = IGNORED_STATUS ;
			break ;
	}

	return status ;

}


void Uart_ctor(Uart *const me)
{
	Active_ctor(&me->super,(StateHandler)&Uart_initial ) ;
	TimeEvent_ctor(&me->start,UART_START_TIMEOUT_SIG,&me->super) ;
	TimeEvent_ctor(&me->stop,UART_STOP_TIMEOUT_SIG,&me->super) ;

}



void Uart_start(uint8_t priority, uint16_t opt) {
	Uart_ctor(&uart) ;
	Active_start(AO_uart,
		         priority,
		         uart_queue,
		         sizeof(uart_queue)/sizeof(uart_queue[0]),
				 uart_stack,
		         sizeof(uart_stack)/sizeof(uart_stack[0]),
				 opt);

}


